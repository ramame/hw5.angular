import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Subscriber } from 'rxjs';

@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],

})

export class MoviesComponent implements OnInit {

  filtering = "newtry";
  name = "no movie";
  displayedColumns: string[] = [ '$id', 'title', 'studio', 'income', 'delete'];
  Studio = ['Soni', 'WB', 'Uni','BV']
  movies = [];
 
 


  toDelete(element)
  {
  element.$see = false;
  }

  toFilter() {
    if (this.filtering  == ""){
    this.db.list('/Movies').snapshotChanges().subscribe(
      Movies => {
        this.movies = [];
        Movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            y["$id"] = movie.key;
            y["$see"] = true;
            this.movies.push(y);
          }
        )
      }
    )}
      else {
        this.db.list('/Studio/'+this.filtering+"/Movies").snapshotChanges().subscribe(
          Movies => {
            this.movies = [];
            Movies.forEach(
              movie => {
                let y = movie.payload.toJSON();
                y["$id"] = movie.key;
                y["$see"] = true;
                this.movies.push(y);
              }
            )
          }
        )

      }
    }

  

 
  constructor(private db: AngularFireDatabase) { }


  ngOnInit() {
    this.db.list('/Movies').snapshotChanges().subscribe(
      Movies => {
        this.movies = [];
        Movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            y["$id"] = movie.key;
            y["$see"] = true;
            this.movies.push(y);
          }
        )
      }
    )
  }

 
}